package pl.oakfusion.bus;

import pl.oakfusion.data.message.Error;
import pl.oakfusion.data.message.Query;
import pl.oakfusion.data.message.QueryResponse;

public abstract class QueryDriven {
    private final MessageBus messageBus;

    public QueryDriven(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    public <Q extends Query> void registerQueryHandler(Class<Q> queryClass, QueryHandler<Q> handler) {
        messageBus.registerMessageHandler(queryClass, handler);
    }

    public <R extends QueryResponse> void post(R response) {
        messageBus.post(response);
    }

    public <E extends Error> void post(E error) {
        messageBus.post(error);
    }
}

