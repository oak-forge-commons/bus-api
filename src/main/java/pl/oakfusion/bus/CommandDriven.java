package pl.oakfusion.bus;

import pl.oakfusion.data.message.Command;
import pl.oakfusion.data.message.Error;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.utils.PreconditionUtils;


public abstract class CommandDriven {
    private final MessageBus messageBus;

    public CommandDriven(MessageBus messageBus) {
        this.messageBus = PreconditionUtils.checkNotNull(messageBus);
    }

    public <C extends Command> void registerCommandHandler(Class<C> commandClass, CommandHandler<C> handler) {
        messageBus.registerMessageHandler(commandClass, handler);
    }

    public <E extends Event> void post(E event) {
        messageBus.post(event);
    }

    public <E extends Error> void post(E error) {
        messageBus.post(error);
    }
}
