package pl.oakfusion.bus;

import pl.oakfusion.data.message.*;
import pl.oakfusion.data.message.Error;

public abstract class MessageDriven {

	private final MessageBus messageBus;

	public MessageDriven(MessageBus messageBus) {
		this.messageBus = messageBus;
	}

	public <M extends Message> void registerMessageHandler(Class<M> messageClass, MessageHandler<M> handler) {
		messageBus.registerMessageHandler(messageClass, handler);
	}

	public <M extends Message> void post(M message) {
		messageBus.post(message);
	}

	public <E extends Error> void post(E error) {
		messageBus.post(error);
	}
}
