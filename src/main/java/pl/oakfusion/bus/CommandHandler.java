package pl.oakfusion.bus;

import pl.oakfusion.data.message.Command;

public interface CommandHandler<C extends Command> extends MessageHandler<C> {
}
