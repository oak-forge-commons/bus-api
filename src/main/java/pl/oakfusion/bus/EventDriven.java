package pl.oakfusion.bus;

import pl.oakfusion.data.message.Command;
import pl.oakfusion.data.message.Error;
import pl.oakfusion.data.message.Event;

public abstract class EventDriven {
    private final MessageBus messageBus;

    public EventDriven(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    public <E extends Event> void registerEventHandler(Class<E> eventClass, EventHandler<E> handler) {
        messageBus.registerMessageHandler(eventClass, handler);
    }

    public <C extends Command> void post(C command) {
        messageBus.post(command);
    }

    public <E extends Error> void post(E error) {
        messageBus.post(error);
    }
}
