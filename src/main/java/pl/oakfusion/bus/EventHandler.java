package pl.oakfusion.bus;

import pl.oakfusion.data.message.*;

public interface EventHandler<E extends Event> extends MessageHandler<E> {}
