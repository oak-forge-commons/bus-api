package pl.oakfusion.bus;

import pl.oakfusion.data.message.Query;

@FunctionalInterface
public interface QueryHandler<Q extends Query> extends MessageHandler<Q> {}
