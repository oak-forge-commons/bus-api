package pl.oakfusion.bus;

import pl.oakfusion.data.message.*;

import java.io.*;
import java.util.*;

/**
 * Basic MessageBus abstraction.
 */
public abstract class MessageBus {
    
	private final Map<Class<?>, List<MessageHandler<? extends Message>>> handlers;

	public MessageBus() {
		this.handlers = new HashMap<>();
	}

	/**
	 * Registers a handlerfor given message type.
	 *
	 * @param messageClass Java class of the message
	 * @param handler message handler to be invokedfor this message type
	 * @param <M> type of the message
	 * @param <H> type of the handler
	 */
	public <M extends Message, H extends MessageHandler<M>> void registerMessageHandler(Class<M> messageClass, H handler) {
		handlers.computeIfAbsent(messageClass, c -> new ArrayList<>()).add(handler);
	}

	/**
	 * Posts a message to the bus.
	 *
	 * Each implementing class is responsible for delivering the message to underling broker by itself.
	 *
	 * @param message a message to be posted
	 * @param <M> type of the message
	 */
	public abstract <M extends Message> void post(M message);

	/**
	 * This is the entry point to message handling.
	 *
	 * This method dispatches any message posted to the bus to each of registered message handlers. Needs to be invoked
	 * 
	 * Number of matching handlers found is returned to the caller after calling them.
	 *
	 * @param message a message object passed to the bus
	 * @param <M> actual message type
	 * @return number of handlers called for message
	 */
	protected <M extends Message> int handle(M message) {
		Class<?> key = message.getClass();
		int handlersCount = 0;
		while (key != null) {
			handlersCount += handle(message, key);
			Class<?> superclass = key.getSuperclass();
			if (superclass == null) {
				break;
			}
			key = superclass.equals(key) ? null : superclass;
		}
		if (handlersCount == 0) {
			System.out.printf("no handler found for message: %s", message.getClass().getName());
		}
		return handlersCount;
	}

    @SuppressWarnings("unchecked")
    private <M extends Message> int handle(M message, Class<?> messageClass) {
		final int[] handlersCount = new int[] {0};
		List<MessageHandler<? extends Message>> handlers = this.handlers.get(messageClass);
		if (handlers != null && !handlers.isEmpty()) {
			handlers.forEach(handler -> {
				try {
                    ((MessageHandler<M>) handler).handle(message);
                    handlersCount[0]++;
				} catch (IOException e) {
					e.printStackTrace();
				}
			});
			return handlersCount[0];
		}
		return handlersCount[0];
	}
}
