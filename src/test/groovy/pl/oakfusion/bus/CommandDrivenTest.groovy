package pl.oakfusion.bus

import pl.oakfusion.data.message.Event
import pl.oakfusion.data.message.Message
import spock.lang.Specification

class CommandDrivenTest extends Specification {

    def 'should throw IllegalArgumentException when constructor param will null '() {
        when:
        new CommandDrivenImpl(null)

        then:
        thrown(IllegalArgumentException)
    }

    def 'command driven should work with given message bus'() {
        given:
        MessageBus messageBus = new MessageBussImpl()
        CommandDrivenImpl commandDriven =  new CommandDrivenImpl(messageBus)

        when:
        commandDriven.post(new EventImpl())

        then:
        noExceptionThrown()
        messageBus.getFlag()
    }

    class CommandDrivenImpl extends CommandDriven{

        CommandDrivenImpl(final MessageBus messageBus) {
            super(messageBus)
        }
    }

    class MessageBussImpl extends MessageBus{
        boolean flag = false

        @Override
        <M extends Message> void post(final M message) {
            flag = true
        }

        boolean getFlag() {
            return flag
        }
    }

    class EventImpl extends Event{}
}
